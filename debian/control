Source: givaro
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>,
           Doug Torrance <dtorrance@debian.org>
Homepage: https://casys.gricad-pages.univ-grenoble-alpes.fr/givaro/
Build-Depends: debhelper-compat (= 13), g++ (>= 4:7), libgmp-dev
Build-Depends-Indep: doxygen, doxygen-latex, graphviz, jdupes, texlive-science
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/math-team/givaro.git
Vcs-Browser: https://salsa.debian.org/math-team/givaro
Rules-Requires-Root: no

Package: libgivaro-dev
Section: libdevel
Architecture: any
Depends: libgivaro9 (= ${binary:Version}), make, ${misc:Depends}
Suggests: libgivaro-doc
Description: arithmetic and algebraic computations - development files
 Givaro is a C++ library for arithmetic and algebraic computations.
 Its main features are implementations of the basic arithmetic of many
 mathematical entities: Primes fields, Extensions Fields, Finite
 Fields, Finite Rings, Polynomials, Algebraic numbers, and Arbitrary
 precision integers and rationals (C++ wrappers over gmp).
 .
 Givaro also provides data-structures and templated classes for the
 manipulation of basic algebraic objects, such as vectors, matrices
 (dense, sparse, structured), univariate polynomials (and therefore
 recursive multivariate).
 .
 It contains different program modules and is fully compatible with
 the LinBox linear algebra library and the Athapascan environment,
 which permits parallel programming.
 .
 This package contains development files for Givaro.

Package: libgivaro9
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: arithmetic and algebraic computations
 Givaro is a C++ library for arithmetic and algebraic computations.
 Its main features are implementations of the basic arithmetic of many
 mathematical entities: Primes fields, Extensions Fields, Finite
 Fields, Finite Rings, Polynomials, Algebraic numbers, and Arbitrary
 precision integers and rationals (C++ wrappers over gmp).
 .
 Givaro also provides data-structures and templated classes for the
 manipulation of basic algebraic objects, such as vectors, matrices
 (dense, sparse, structured), univariate polynomials (and therefore
 recursive multivariate).
 .
 It contains different program modules and is fully compatible with
 the LinBox linear algebra library and the Athapascan environment,
 which permits parallel programming.
 .
 This package contains runtime files for Givaro.

Package: libgivaro-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Documentation for Givaro
 Givaro is a C++ library for arithmetic and algebraic computations.
 Its main features are implementations of the basic arithmetic of many
 mathematical entities: Primes fields, Extensions Fields, Finite
 Fields, Finite Rings, Polynomials, Algebraic numbers, and Arbitrary
 precision integers and rationals (C++ wrappers over gmp).
 .
 Givaro also provides data-structures and templated classes for the
 manipulation of basic algebraic objects, such as vectors, matrices
 (dense, sparse, structured), univariate polynomials (and therefore
 recursive multivariate).
 .
 It contains different program modules and is fully compatible with
 the LinBox linear algebra library and the Athapascan environment,
 which permits parallel programming.
 .
 This package contains Documentation for Givaro.

Package: givaro-user-doc
Section: doc
Architecture: all
Depends: libgivaro-doc, ${misc:Depends}
Description: User Documentation for Givaro (obsolete)
 Givaro is a C++ library for arithmetic and algebraic computations.
 Its main features are implementations of the basic arithmetic of many
 mathematical entities: Primes fields, Extensions Fields, Finite
 Fields, Finite Rings, Polynomials, Algebraic numbers, and Arbitrary
 precision integers and rationals (C++ wrappers over gmp).
 .
 Givaro also provides data-structures and templated classes for the
 manipulation of basic algebraic objects, such as vectors, matrices
 (dense, sparse, structured), univariate polynomials (and therefore
 recursive multivariate).
 .
 It contains different program modules and is fully compatible with
 the LinBox linear algebra library and the Athapascan environment,
 which permits parallel programming.
 .
 This is a transitional dummy package. The 'givaro-user-doc' package
 has been renamed to 'libgivaro-doc', which has been installed
 automatically. This 'givaro-user-doc' package can be safely removed
 from the system if no other package depends on it.

Package: givaro-dev-doc
Section: doc
Architecture: all
Depends: libgivaro-doc, ${misc:Depends}
Description: Developer Documentation for Givaro (obsolete)
 Givaro is a C++ library for arithmetic and algebraic computations.
 Its main features are implementations of the basic arithmetic of many
 mathematical entities: Primes fields, Extensions Fields, Finite
 Fields, Finite Rings, Polynomials, Algebraic numbers, and Arbitrary
 precision integers and rationals (C++ wrappers over gmp).
 .
 Givaro also provides data-structures and templated classes for the
 manipulation of basic algebraic objects, such as vectors, matrices
 (dense, sparse, structured), univariate polynomials (and therefore
 recursive multivariate).
 .
 It contains different program modules and is fully compatible with
 the LinBox linear algebra library and the Athapascan environment,
 which permits parallel programming.
 .
 This is a transitional dummy package, Debian no longer ships Developer
 Documentation for Givaro. This 'givaro-dev-doc' package can be safely
 removed from the system if no other package depends on it.
